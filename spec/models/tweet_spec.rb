require 'rails_helper'

RSpec.describe Tweet, type: :model do
  let(:user) { create(:user) }

  describe "#valid?" do
    context "when I have the same body within the 24 hours" do
      subject { tweet.valid? }

      let(:tweet) { build(:tweet, user: user, body: "hello") }

      before { create(:tweet, user: user, body: "hello") }

      it { is_expected.to be false }
    end

    context "when I have the same body with more than 180 digits" do
      subject { tweet.valid? }

      let(:tweet) { build(:tweet, user: user, body: "3" * 181  ) }

      it { is_expected.to be false }
    end

    context "when I have the same body with less/equal than 180 digits and the body is new within the 24hrs" do
      subject { tweet.valid? }

      let(:tweet) { build(:tweet, user: user, body: "3" * 180  ) }

      it { is_expected.to be true }
    end
  end 
end
