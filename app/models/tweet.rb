class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  validate :body_within_24hrs

  def body_within_24hrs
    tweets = Tweet.where(
      user_id: user.id,
      body: body,
      created_at: 24.hours.ago..Time.now,
    )

    errors.add(:base, "You cannot use the same body within 24 hrs") if tweets.present?
  end
end
